#include "pch.h"
#include "../Quarto/Piece.cpp"
#include "../Quarto/Board.cpp"

TEST(TestCaseName, TestName) {
	Piece piece(Piece::Body::Hollow, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Square);

	EXPECT_EQ(Piece::Body::Hollow, piece.GetBody());
	EXPECT_TRUE(Piece::Color::Dark == piece.GetColor());
	EXPECT_TRUE(Piece::Height::Short == piece.GetHeight());
	EXPECT_TRUE(Piece::Shape::Square == piece.GetShape());
}

TEST(BoardTest, TestName) {
	Board board;
	Piece piece(Piece::Body::Hollow, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Square);
	Board::Position position{ 1, 1 };
	board[position] = piece;

	EXPECT_TRUE(board[position].has_value());
	//EXPECT_EQ(board[position].value(), piece); //== operator must be implemented for Piece class
}
