#pragma once
#include <string>
#include <iostream>

#ifdef LOGGING_EXPORTS
#define LOGGING_API _declspec(dllexport)
#else
#define LOGGING_API _declspec(dllimport)
#endif

class LOGGING_API Logger
{
public:
	enum class Level : char
	{
		Info = 1,
		Warning,
		Error
	};

public:
	Logger(std::ostream& os, Level minimumLevel = Level::Info);

	void log(const std::string& message, Level level);

	void setMinimumLogLevel(Level minimumLevel);

private:
	std::ostream& os;
	Level minimumLevel;
};

std::ostream& operator<<(std::ostream& str, Logger::Level type)
{
	switch (type)
	{
	case Logger::Level::Info: str << "Info"; break;
	case Logger::Level::Warning: str << "Warning"; break;
	case Logger::Level::Error: str << "Error"; break;
	default: break;
	}
	return str;
}