#include <iostream>
#include <fstream>

#include "Game.h"
#include "../Logging/Logger.h"

int main()
{
	//Logger logger(std::cout);
	//logger.log("game has started", Logger::Level::Info);

	//std::ofstream of("quarto.log", std::ios::app);
	//Logger fileLogger(of);
	//fileLogger.log("this is a log", Logger::Level::Info);

	Game game;
	game.Run();

	return 0;
}
