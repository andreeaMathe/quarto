#include "Piece.h"

Piece::Piece(Body body, Color color, Height height, Shape shape) :
	m_body(body),
	m_color(color),
	m_height(height),
	m_shape(shape)
{
	// Empty
	static_assert(sizeof(*this) <= 1, "This class should be 1 byte in size");
}

Piece::Piece()
	//: m_body(Piece::Body::None)
	//, m_color(Piece::Color::None)
	//, m_height(Piece::Height::None)
	//, m_shape(Piece::Shape::None)
{
}

Piece::Piece(const Piece& other)
{
	*this = other;
}

Piece::Piece(Piece&& other)
{
	*this = std::move(other);
}

Piece::~Piece()
{
	//m_body = Piece::Body::None;
	//m_color = Piece::Color::None;
	//m_height = Piece::Height::None;
	//m_shape = Piece::Shape::None;
}

Piece& Piece::operator= (const Piece& other)
{
	m_body = other.m_body;
	m_color = other.m_color;
	m_height = other.m_height;
	m_shape = other.m_shape;

	return *this;
}

Piece& Piece::operator= (Piece&& other)
{
	m_body = other.m_body;
	m_color = other.m_color;
	m_height = other.m_height;
	m_shape = other.m_shape;

	//other.m_body = Body::None;
	//other.m_color = Color::None;
	//other.m_height = Height::None;
	//other.m_shape = Shape::None;

	new(&other) Piece;

	return *this;
}

Piece::Body Piece::GetBody() const
{
	return m_body;
}

Piece::Color Piece::GetColor() const
{
	return m_color;
}

Piece::Height Piece::GetHeight() const
{
	return m_height;
}

Piece::Shape Piece::GetShape() const
{
	return m_shape;
}

std::ostream& operator<<(std::ostream& os, const Piece& piece)
{
	return os <<
		static_cast<int>(piece.m_body) <<
		static_cast<int>(piece.m_color) <<
		static_cast<int>(piece.m_height) <<
		static_cast<int>(piece.m_shape);
}
