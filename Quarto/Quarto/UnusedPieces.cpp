#include "UnusedPieces.h"

#include <sstream>
#include <array>
#include <algorithm>
#include <bitset>

UnusedPieces::UnusedPieces()
{
	GenerateUnusedPieces();
}

Piece UnusedPieces::PickPiece(const std::string& pieceName)
{
	auto piece = m_unusedPieces.extract(pieceName);
	if (piece)
		return std::move(piece.mapped());
	else
		throw "Piece not found";
}

void UnusedPieces::GenerateUnusedPieces()
{
	const uint8_t kPoolSize = 8;
	const uint8_t kPermutations = 5;
	const uint8_t kOnePermutationSize = 4;
	std::array<uint8_t, kPoolSize> pool = { 0, 0, 0, 0, 1, 1, 1, 1 };

	for (uint8_t permutationStartIndex = 0; permutationStartIndex < kPermutations; ++permutationStartIndex)
	{
		auto begin = pool.begin() + permutationStartIndex;
		auto end = pool.begin() + permutationStartIndex + kOnePermutationSize;

		do
		{
			InsertPiece(Piece(static_cast<Piece::Body>(*begin), static_cast<Piece::Color>(*(begin + 1)),
				static_cast<Piece::Height>(*(begin + 2)), static_cast<Piece::Shape>(*(begin + 3))));

			std::cout << static_cast<int>(*begin) << static_cast<int>(*(begin + 1)) <<
				static_cast<int>(*(begin + 2)) << static_cast<int>(*(begin + 3)) << std::endl;

		} while (std::next_permutation(begin, end));
		std::cout << "new permutaion" << std::endl;
	}
}

//void UnusedPieces::GenerateUnusedPieces()
//{
	//InsertPiece(Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Square));
	//InsertPiece(Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round));
	//InsertPiece(Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square));
	//InsertPiece(Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round));
	//InsertPiece(Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Short, Piece::Shape::Square));
	//InsertPiece(Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Short, Piece::Shape::Round));
	//InsertPiece(Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Square));
	//InsertPiece(Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Round));

	//InsertPiece(Piece(Piece::Body::Hollow, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Square));
	//InsertPiece(Piece(Piece::Body::Hollow, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round));
	//InsertPiece(Piece(Piece::Body::Hollow, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square));
	//InsertPiece(Piece(Piece::Body::Hollow, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round));
	//InsertPiece(Piece(Piece::Body::Hollow, Piece::Color::Light, Piece::Height::Short, Piece::Shape::Square));
	//InsertPiece(Piece(Piece::Body::Hollow, Piece::Color::Light, Piece::Height::Short, Piece::Shape::Round));
	//InsertPiece(Piece(Piece::Body::Hollow, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Square));
	//InsertPiece(Piece(Piece::Body::Hollow, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Round));
//}

void UnusedPieces::InsertPiece(const Piece& piece)
{
	std::stringstream stringStream;
	stringStream << piece;
	m_unusedPieces.insert(std::make_pair(stringStream.str(), piece));
}

std::ostream& operator<<(std::ostream& os, const UnusedPieces& unusedPieces)
{
	os << "Unused pieces : ";
	for (auto it : unusedPieces.m_unusedPieces)
	{
		os << it.first << " ";
	}
	os << std::endl;

	return os;
}
