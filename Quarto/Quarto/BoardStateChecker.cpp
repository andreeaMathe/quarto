#include "BoardStateChecker.h"
#include <numeric>

BoardStateChecker::State BoardStateChecker::Check(const Board& b, const Board::Position& position)
{
	Board& board = const_cast<Board&>(b);
	const auto& [lineIndex, columnIndex] = position;

    if (board.IsFull())
        return State::Draw;

    return State::None;
}
